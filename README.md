# C# TCP Test #

This repository contains the would-be library for the TCP-based API. Since it was created mainly to test the server of the main project (located [here](https://bitbucket.org/vrterra/irrlicht)) it also contains a small console application which utilizes the library to connect and send a few messages. The library is located in the `VrTerraClient` subfolder while the console application used for testing is located in the `tcp-csharp-test` subfolder.

## Building

The project is written using Visual Studio and the solution files are committed along with the code so it should be easy to open and compile on your own. Simply open the `.sln` file from the subfolder of your wish and then simply compile (or run if you're testing the console application.)

## Running

When ran the console application will, by default, attempt to connect to the server as if it were running on your local machine (127.0.0.1). It will then crash (unless you have the server running) because it does not have any error handling.

To connect to an arbitrary IP-address using the console application simply run it with the IP/hostname as an argument, as so:  
`.\tcp-csharp-test.exe [IP address]`  
e.g.  
`.\tcp-csharp-test.exe 192.168.0.50`

## Using the library

Examples of using the library can be seen in the source code of the console application. The library also has fairly decent documentation on the public interface so you can read information from that. Its main use-case was Unity but we were pressed on time and Unity-compatibility has, sadly, not been tested. Although it should theoretically work as it was rewritten to run under .NET framework 2.0, which is the version Unity uses (although in mono for cross-platform compatibility.)

## Dependencies

Dependencies should be downloaded automatically using NuGet. In the case it is not, here it is:

http://www.newtonsoft.com/json

## License

Copyright (c) 2016 Mårten Nordheim 


Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Todo
- Test with Unity