﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VrTerraClient;

namespace tcp_csharp_test
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var host = "127.0.0.1";
            if (args.Length > 0)
            {
                host = args[0];
            }
            var client = new VrTerraClient.VrTerraClient(host, 24242);

            client.RequestTerrain();
            client.InitializeMarker();

            List<Marker> markerList;
            while ((markerList = client.GetMarkers()).Count <= 0 && client.IsRunning)
            {
                Thread.Sleep(150);
            }
            var marker = markerList[0];
            marker.X += 50;
            marker.Y += 100;
            client.SetMarkerPosition(marker.Id, marker.X, marker.Y);

            var tokenSource = new CancellationTokenSource();
            var running = true;
            Task.Run(() =>
            {
                while (running)
                {
                    if (Console.KeyAvailable)
                    {
                        var msg = Console.In.ReadLine();
                        if (msg == "q")
                            running = false;
                        else if (msg.StartsWith("m"))
                        {
                            var strings = msg.Split(' ');
                            if (strings.Length == 3)
                            {
                                var x = int.Parse(strings[1]);
                                var y = int.Parse(strings[2]);
                                marker.X += x;
                                marker.Y += y;
                                client.SetMarkerPosition(marker.Id, marker.X, marker.Y);
                            }
                        }
                    }

                    if (tokenSource.IsCancellationRequested)
                    {
                        return;
                    }
                    Thread.Sleep(150);
                }
            }, tokenSource.Token);

            while (client.IsRunning && running)
            {
                Thread.Sleep(150);
            }

            SaveHeightmap(client.GetTerrainCopy());
            client.Shutdown();

            tokenSource.Cancel();
            Console.WriteLine("the end");
        }

        private static void SaveHeightmap(Terrain terrain)
        {
            if (terrain == null)
            {
                return;
            }
            var minVal = terrain.Heightmap.Min();
            var maxVal = terrain.Heightmap.Max();

            // if maxVal is 0 (or almost 0), we pretend it's 1.
            if (Math.Abs(maxVal) < 0.001f)
            {
                maxVal = 0.001f;
            }
            using (var bitmap = new Bitmap(terrain.Width, terrain.Height))
            {
                Console.WriteLine($"Rows: {terrain.Width}\nColumns: {terrain.Height}");
                for (var y = 0; y < terrain.Height; y++)
                {
                    for (var x = 0; x < terrain.Width; x++)
                    {
                        Color color;
                        if (Math.Abs(maxVal - minVal) < 0.00001)
                        {
                            color = Color.FromArgb(0, 0, 0);
                        }
                        else
                        {
                            var value = (int)(((terrain[x, y] - minVal) / (maxVal - minVal)) * 255);
                            color = Color.FromArgb(value, value, value);
                        }
                        bitmap.SetPixel(x, y, color);
                    }
                }
                bitmap.Save("terrain.bmp");
            }
            Console.WriteLine("Saved terrain to terrain.bmp");
        }
    }
}