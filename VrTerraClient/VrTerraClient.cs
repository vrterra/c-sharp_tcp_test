﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace VrTerraClient
{
    /// <summary>
    /// Client for the sandbox, runs a thread in the background after being constructed.
    /// </summary>
    public class VrTerraClient
    {
        private const string HeaderSeparator = "\r\n";
        private readonly TcpClient _client;
        private readonly StreamReader _streamReader;
        private readonly StreamWriter _streamWriter;
        private readonly Thread _thread;
        private readonly Queue<string> _messageQueue = new Queue<string>();
        private Terrain _terrain = new Terrain(1, 1, new[] {0.0f});
        private readonly MarkerManager _markerManager = new MarkerManager();

        /// <summary>
        /// Constructor for VrTerraClient. Opens a connection to hostname:port and then starts a thread conversing with the server
        /// </summary>
        /// <param name="hostname">the hostname of the server</param>
        /// <param name="port">the port which the server listens on</param>
        /// <exception cref="SocketException">when the connection fails</exception>
        public VrTerraClient(string hostname, ushort port)
        {
            _client = new TcpClient(hostname, port);
            _streamReader = new StreamReader(_client.GetStream());
            _streamWriter = new StreamWriter(_client.GetStream());
            _thread = new Thread(Loop);
            _thread.Start();
        }

        /// <summary>
        /// True if it's running, false if it has shut down or is currently shutting down
        /// </summary>
        public bool IsRunning { get; private set; } = true;

        /// <summary>
        /// Put a json-encoded message in queue to be sent
        /// </summary>
        /// <param name="message">the message to send</param>
        private void EnqueueMessage(string message)
        {
            lock (_messageQueue)
            {
                _messageQueue.Enqueue(message);
            }

            // @todo: remove
            Console.WriteLine("Queued a message");
        }

        /// <summary>
        /// Disconnects from the server, stops the client and waits for the second thread to shut down
        /// </summary>
        public void Shutdown()
        {
            var wasRunning = IsRunning;
            IsRunning = false;

            if (Thread.CurrentThread != _thread)
            {
                // avoid waiting on current thread (deadlock)
                _thread.Join();
            }

            if (!wasRunning)
            {
                return;
            }

            var dictionary = new Dictionary<string, dynamic> {{"flag", Flags.Disconnect}};
            var disconnectMsg = JsonConvert.SerializeObject(dictionary);
            SendJsonData(disconnectMsg);

            _streamWriter.Close();
            _streamReader.Close();
            _client.Close();
        }

        /// <summary>
        /// Get a copy of this instance's Terrain instance for you to use
        /// </summary>
        /// <returns>A copy of the most recent Terrain instance</returns>
        public Terrain GetTerrainCopy()
        {
            return new Terrain(_terrain);
        }

        /// <summary>
        /// Send json encoded data to the server
        /// </summary>
        /// <param name="jsonMessage">the message</param>
        private void SendJsonData(string jsonMessage)
        {
            _streamWriter.Write(jsonMessage.Length + HeaderSeparator);
            _streamWriter.Write(jsonMessage);

            try
            {
                _streamWriter.Flush();
                // @todo remove
                Console.Error.WriteLine("Sent a message");
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"Encountered exception while flushing stream: {e.Message}");
            }
        }

        /// <summary>
        /// Try to read a message from the stream
        /// </summary>
        /// <param name="jsonContent">the json message if any was ready</param>
        /// <returns>true if there was a message available, false otherwise</returns>
        private bool TryReadMessage(out Dictionary<string, object> jsonContent)
        {
            jsonContent = null;
            if (_client.Available <= 0)
            {
                // There's currently nothing to be read on the stream
                return false;
            }

            var incoming = _streamReader.ReadLine();

            if (incoming == null)
            {
                return false;
            }

            var header = incoming;
            int amountToRead;
            var success = int.TryParse(header, out amountToRead);
            if (!success)
            {
                // Didn't get an integer out from the header
                return false;
            }

            var exactBuffer = new char[amountToRead];
            _streamReader.ReadBlock(exactBuffer, 0, amountToRead);
            var content = new string(exactBuffer);
            if (content.Length != amountToRead)
            {
                // Didn't get everything we should - the json string is very likely invalid
                return false;
            }
            jsonContent = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(content);
            return true;
        }

        /// <summary>
        /// Send and receive data until stopped
        /// </summary>
        private void Loop()
        {
            while (IsRunning)
            {
                // Send data
                string msg = null;
                lock (_messageQueue)
                {
                    if (_messageQueue.Count > 0)
                    {
                        msg = _messageQueue.Dequeue();
                    }
                }
                if (!string.IsNullOrEmpty(msg))
                {
                    SendJsonData(msg);
                }

                // Receive data
                Dictionary<string, object> jsonContent;
                var messageRead = TryReadMessage(out jsonContent);
                if (messageRead)
                {
                    HandleMessage(jsonContent);
                }
                
                if (!string.IsNullOrEmpty(msg) && !messageRead && IsRunning)
                {
                    // Sleep 150ms if we didn't do anything this iteration.. (to not run the cpu at 100%)
                    Thread.Sleep(150);
                }
            }
        }

        /// <summary>
        /// Handle an incoming message
        /// </summary>
        /// <param name="jsonContent">the message</param>
        private void HandleMessage(IDictionary<string, object> jsonContent)
        {
            Debug.Assert(jsonContent != null, "jsonContent != null");
            var flag = (Flags)Enum.ToObject(typeof(Flags), jsonContent["flag"]);
            switch (flag)
            {
                case Flags.Disconnect:
                    Shutdown();
                    break;

                case Flags.HeightmapRequest:
                    Console.Error.WriteLine("HeightmapRequest is not used on client-side");
                    break;

                case Flags.HeightmapSend:
                    var rows = Convert.ToInt32(jsonContent["rows"]);
                    var columns = Convert.ToInt32(jsonContent["columns"]);
                    var heightmap = ((JArray)jsonContent["heightmap"]).ToObject<float[]>();

                    _terrain = new Terrain(columns, rows, heightmap);
                    break;

                case Flags.MarkerInitialize:
                    Console.Error.WriteLine("MarkerInitialize is not used on client-side");
                    break;

                case Flags.MarkerPositionSend:
                    Console.Error.WriteLine("MarkerPositionSend is not used on client-side");
                    break;

                case Flags.MarkerInitializeResponse:
                    var id = Convert.ToInt32(jsonContent["id"]);
                    var marker = new Marker(id);
                    _markerManager.AddMarker(marker);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Tell the server that you need a new marker
        /// </summary>
        public void InitializeMarker()
        {
            var dictionary = new Dictionary<string, object>
            {
                {"flag", Flags.MarkerInitialize}
            };
            var msg = JsonConvert.SerializeObject(dictionary);
            EnqueueMessage(msg);
        }

        /// <summary>
        /// Request a new Terrain reading from the server
        /// </summary>
        public void RequestTerrain()
        {
            var dictionary = new Dictionary<string, object> {{"flag", Flags.HeightmapRequest}};
            var msg = JsonConvert.SerializeObject(dictionary);
            EnqueueMessage(msg);
        }

        /// <summary>
        /// Get the list of markers assigned to this client
        /// </summary>
        /// <returns>a list of markers</returns>
        public List<Marker> GetMarkers()
        {
            return _markerManager.GetMarkerList();
        }

        /// <summary>
        /// Tell the server about a marker's new position
        /// </summary>
        /// <param name="id">the marker's id</param>
        /// <param name="x">it's x-coordinate</param>
        /// <param name="y">it's y-coordinate</param>
        public void SetMarkerPosition(int id, int x, int y)
        {
            var str = JsonConvert.SerializeObject(new Dictionary<string, dynamic>
            {
                {"flag", Flags.MarkerPositionSend},
                {"id", id},
                {"x", x},
                {"y", y}
            });
            EnqueueMessage(str);
        }
    }
}