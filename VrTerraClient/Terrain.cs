﻿using System;
using System.Collections.Generic;

namespace VrTerraClient
{
    /// <summary>
    /// A basic class to contain information about a heightmap
    /// </summary>
    public class Terrain
    {
        private readonly int _width;
        private readonly int _height;
        private List<float> _heightmap;

        /// <summary>
        /// The width (x-axis) of the heightmap
        /// </summary>
        public int Width => _width;

        /// <summary>
        /// The height (z-axis) of the heightmap
        /// </summary>
        public int Height => _height;

        /// <summary>
        /// The total size of the heightmap (width * height)
        /// </summary>
        public int Length => _width * _height;

        /// <summary>
        /// A copy of the heightmap, changes will not be reflected
        /// </summary>
        public List<float> Heightmap => new List<float>(_heightmap);

        /// <summary>
        /// Get the value stored at a specified index
        /// </summary>
        /// <param name="i">The 0-based index</param>
        /// <returns>The value stored at index 'i'</returns>
        public float this[int i] => Heightmap[i];

        /// <summary>
        /// Get the value stored at a specified 2-dimensional index
        /// </summary>
        /// <param name="x">The index on the x-axis</param>
        /// <param name="y">The index on the y-axis</param>
        /// <returns>the value stored at in the heightmap at the given coordinates</returns>
        public float this[int x, int y]
        {
            get
            {
                if (x > _width)
                {
                    throw new IndexOutOfRangeException(
                        $"'{nameof(x)}' ({x}) is greater than the width ({Width}) of the heightmap!");
                }
                if (y > _height)
                {
                    throw new IndexOutOfRangeException(
                        $"'{nameof(y)}' ({y}) is greater than the height ({Height}) of the heightmap!");
                }
                return _heightmap[x + y * _width];
            }
        }

        /// <summary>
        /// Constructor for Terrain
        /// </summary>
        /// <param name="width">The width of the heightmap (x-axis)</param>
        /// <param name="height">The height of the heightmap (z-axis)</param>
        /// <param name="heightmap">An array of floats to copy in as the heightmap</param>
        internal Terrain(int width, int height, float[] heightmap)
        {
            _width = width;
            _height = height;
            CopyHeightmapFromArray(heightmap);
        }

        /// <summary>
        /// Creates a copy of a Terrain
        /// </summary>
        /// <param name="terrain">the instance to copy from</param>
        public Terrain(Terrain terrain)
        {
            _height = terrain.Height;
            _width = terrain.Width;
            _heightmap = terrain.Heightmap;
        }

        /// <summary>
        /// Overwrite the current heightmap of this object with an array of floats
        /// </summary>
        /// <param name="heightmap"></param>
        public void CopyHeightmapFromArray(float[] heightmap)
        {
            if (heightmap.Length != Width * Height)
            {
                throw new ArgumentException(
                    $"Heightmap argument is not of the right size! Expected {Width * Height}, got {heightmap.Length}!");
            }
            _heightmap = new List<float>(heightmap);
        }
    }
}