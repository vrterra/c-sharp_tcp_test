﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VrTerraClient
{
    public class MarkerManager
    {
        private readonly List<Marker> _markers = new List<Marker>();

        public void AddMarker(Marker m)
        {
            lock (_markers)
            {
                _markers.Add(m);
            }
        }
        
        public Marker? GetMarker(int id)
        {
            lock (_markers)
            {
                foreach (var marker in _markers)
                {
                    if (marker.Id.Equals(id))
                    {
                        return marker;
                    }
                }
            }
            return null;
        }

        public List<Marker> GetMarkerList()
        {
            lock (_markers)
            {
                return new List<Marker>(_markers);
            }
        }
    }
}
