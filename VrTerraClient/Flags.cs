﻿namespace VrTerraClient
{
    public enum Flags
    {
        // Connection related
        Disconnect = 0x0,

        // Heightmap related
        HeightmapRequest = 0x1,
        HeightmapSend = 0x2,

        // Marker related
        MarkerInitialize = 0x3,
        MarkerPositionSend = 0x4,
        MarkerInitializeResponse = 0x5
    }
}