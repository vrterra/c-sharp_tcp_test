﻿namespace VrTerraClient
{
    public struct Marker
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Id { get; set; }

        public Marker(int id)
        {
            this.Id = id;
            X = 0;
            Y = 0;
        }
    }
}